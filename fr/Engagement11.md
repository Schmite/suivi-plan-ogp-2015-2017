# Engagement 11 : Coproduire avec la société civile les registres-clés de données essentielles à la société et à l'économie

## Institutions porteuses
- Secrétariat d'État chargé de la Réforme de l'État et de la Simplification
- Secrétariat d'État chargé du Numérique

## Enjeux : 
**De nouvelles formes de coopérations entre les autorités publiques et les citoyens permettent désormais de créer de nouveaux biens communs**, indispensables au service public, à la société et à l'économie, d'une manière plus rapide, plus efficace et moins coûteuse que par le passé.

## Description de l'engagement : 

**Multiplier les coopérations entre acteurs publics et société civile pour la constitution d'infrastructures de données essentielles et de registres-clés de données.**

[En savoir plus](http://gouvernement-ouvert.etalab.gouv.fr/content/fr/consulter-concerter-co-produire/action-publique/engagement-11.html)
 
## Description des résultats :

**Actions** | **Résultats** | **Prochaines étapes** | **Statut**
--- | --- | --- | ---
Multiplier les coopérations entre acteurs publics et société civile pour la constitution d’infrastructures de données essentielles et de registres-clé de données. | Le [projet de loi pour une République numérique]( https://www.legifrance.gouv.fr/affichLoiPreparation.do?idDocument=JORFDOLE000031589829&type=general&typeLoi=proj&legislature=14) donnera des obligations législatives sur le service public de la donnée. Le travail sur le décret a commencé. Des coopérations sont prévues ou déjà en cours autour des registres clés  : la [base adresse nationale](https://www.data.gouv.fr/fr/datasets/ban-base-adresse-nationale/) (BAN) fait l'objet d'une coopération continue entre l'IGN, La Poste, le SGMAP, l'association OpenStreetMap France et des collectivités ; la base des établissements recevant du public (ERP) est en projet à l'IGN et au SGMAP et l'association Open Data France travaille à la standardisation de ces données dans les collectivités territoirales ; concernant le registre national des associations (RNA), le site [DataAsso](http://www.dataasso.com/apropos), financé par le Programme d'Investissements d'Avenir, présente toutes les associations françaises relevant de la loi 1901 en partenariat avec le ministère de la Ville, de la Jeunesse et des Sports et le ministère de l'Intérieur ; le registre national des élus est disponible sur demande auprès du bureau des élections du ministère de l'Intérieur et [a été enrichi](https://www.data.gouv.fr/fr/datasets/liste-des-maires/) par IdéesLibres.org. | Mener un travail de concertation pour déterminer quels sont les registres clés qui seront inclus dans le décret. | Partiel
