# Rapport d'autoévaluation à mi-parcours du Plan d'action pour la France 2015-2017 "Pour une action publique transparente et collaborative"

Ce livre est une version numérique du rapport d'autoévaluation à mi-parcours du [Plan d'action national pour la France 2015-2017](http://gouvernement-ouvert.etalab.gouv.fr/content/fr/index.html),
élaboré dans le cadre du [Partenariat pour un gouvernement ouvert (PGO)](http://www.opengovpartnership.org/). Il présente, engagement par engagement, l'avancement des actions prises par les ministères en matière
de gouvernement ouvert.

Ce livre en ligne est ouvert à contribution citoyenne (forum intégré) : chacun peut ainsi proposer des modifications, suggérer de nouveaux exemples de réussites et solliciter de nouveaux types de collaborations
entre la société civile et l'administration. Cette espace restera ouvert jusqu'en juillet 2017, date à laquelle la France remettra un rapport d'autoévaluation final.

La version PDF de ce livre est également disponible : 
- [version initiale avant la consultation de juin 2016](documentation/suivi-plan-ogp-2015-2017_fr-version-initiale.pdf)
- [version enrichie de juillet 2016](http://suivi-gouvernement-ouvert.etalab.gouv.fr/fr/suivi-plan-ogp-2015-2017_fr.pdf)

Le code source de ce livre numérique est disponible dans le dépôt suivant : [FramaGit](https://framagit.org/etalab/suivi-plan-ogp-2015-2017/).

Pour toute question ou suggestion : <gouvernement-ouvert@etalab.gouv.fr> 

