# 6. Conclusion : enseignements et prochaines étapes

## 6.1. La démarche de gouvernement ouvert initiée par la France depuis son adhésion au Partenariat pour un gouvernement ouvert en 2014 transforme les institutions et administrations vers une action publique plus transparente et concertée 

### Plusieurs enseignements doivent être dégagés de cette première année de mise en oeuvre du plan d'action : 

- **L'implication en amont d'un nombre significatif de ministères dans le Plan d'action national est un facteur clé de succès**. 
    * Elle a en effet permis d'entraîner l'ensemble des administrations dans la même orientation et d'accélérer de nombreux projets. L'ouverture du code source de la calculette impôt en est un exemple significatif.
    * Le Plan d'action national offre un socle stable et lisible pour la modernisation des politiques publiques et leur ouverture à la contribution citoyenne.
    * Des alliances gouvernement-société civile ont d'ores et déjà donné naissance à des projets concrets, comme l'initiative Open Law autour de l'ouverture du droit.
- **De nombreux engagements ont été inscrits dans la loi** : projets et propositions de loi relatifs à la vie publique et économique, à la modernisation de la fonction publique, à la réforme territoriale, au numérique, à la biodiversité, etc. Le cadre législatif de l'ouverture de l'action publique s'enrichit : la future création d'un service public de la donnée, la constitution de registres publics sur la transparence du lobbying, le renforcement des règles déontologiques pour les agents publics, la lutte contre la corruption sont des composantes majeures du gouvernement ouvert et sont désormais inscrites dans la loi ou soumises au débat législatif.
- **La France renforce sa présence dans des instances ou actions de coopération internationales**, avec par exemple le groupe "Contracting 5" sur la commande publique ([en savoir plus](https://www.etalab.gouv.fr/sommet-anti-corruption-la-france-sengage-sur-des-registres-publics-et-standards-ouverts)).

### Des axes de progrès ont été d'ores et déjà notés et pourront être à intégrées dans les futures initiatives de gouvernement ouvert, dont : 
- L'élaboration d'engagements qui soient davantage mesurables par des indicateurs quantitatifs et qualitatifs ;
- La simplification du suivi des engagements entre les administrations et son ouverture à la société civile ;
- La plus forte implication de profils techniques (développeurs, datascientists, architectes, etc.) dans la mise en oeuvre du plan d'action.


## 6.2. Afin de propulser, dans la durée, les actions de la France en faveur du gouvernement ouvert, plusieurs étapes sont identifiées : 

- La démarche "Ministère ouvert" ([en savoir plus](https://www.etalab.gouv.fr/plan-daction-pgo-un-premier-evenement-ministere-ouvert-le-21-juin-2016)) : rencontres régulières entre représentants des administrations et représentants de la société civile pour suivre la mise en oeuvre des engagements. Une première édition préfiguratrice est prévue le 21 juin 2016 au Secrétariat d'Etat chargé de la Réforme de l'Etat et de la Simplification ; ;
- Un dispositif de consultation en continu tant sur le fond des actions que sur la méthode ;
- La coordination des actions françaises avec l'agenda du Partenariat pour un gouvernement ouvert. Parmi les prochains rendez-vous : 
    * A l'occasion de l'Assemblée générale des Nations Unies fin septembre 2016, la France publiera la version anglaise de son rapport d'autoévaluation à mi-parcours.
    * Le rapport du mécanisme indépendant d'évaluation sera publié fin septembre 2016.
    * Le Sommet mondial du PGO sera organisé à Paris du 7 au 9 décembre 2016 : un point d'étape sur le plan d'action sera réalisé, et les dispositifs construits pour le suivi seront intégrés à la boîte à outils du gouvernement ouvert.
    * Le deuxième Plan d'action national et le rapport final d'autoévaluation seront publiés à l'été 2017.
    * Le rappport du mécanisme indépendant d'évaluation sera publié fin septembre 2017.
    * En continu, société civile et administrations travailleront à l'élaboration du prochain plan d'action.
