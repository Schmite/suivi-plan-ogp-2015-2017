# 2.1. Consultation lors de l'élaboration du Plan d'action national

L'élaboration du Plan d'action a été coordonnée par la [mission Etalab](http://www.etalab.gouv.fr/), au sein du [secrétariat général pour la modernisation de l'action publique](http://modernisation.gouv.fr/), entre octobre 2014 et juin 2015, à travers plusieurs canaux de consultations :
- Une consultation en ligne, organisée par le [Conseil national du numérique](http://www.cnnumerique.fr/), entre le 3 novembre 2014 et le 28 février 2015 : 
    * [Les débats de la consultation ainsi que la synthèse des contributions](https://contribuez.cnnumerique.fr/debat/open-gov-comment-faire-progresser-la-transparence-de-l%E2%80%99action-publique-et-la-participation) sont toujours disponibles en ligne. Au sein du thème dédié à la transformation de l'action publique, **deux consultations ont été consacrées entièrement à l'« [Open Data](http://contribuez.cnnumerique.fr/debat/open-data-une-d%C3%A9mocratie-plus-ouverte-et-de-nouveaux-biens-communs) » et au « [Gouvernement Ouvert](http://contribuez.cnnumerique.fr/debat/open-gov-comment-faire-progresser-la-transparence-de-l%E2%80%99action-publique-et-la-participation) »**. Les contributions publiées dans d'autres sujets de consultation (ex : [Stratégie technologique de l'Etat et services publics](http://contribuez.cnnumerique.fr/debat/strat%C3%A9gie-technologique-de-letat-et-services-publics)) ont également nourri la réflexion sur le gouvernement ouvert. 
    * Les données de la consultation ont été publiées en [open data sur data.gouv.fr](https://www.data.gouv.fr/fr/datasets/concertation-nationale-sur-le-numerique-2/). Les propositions qui sont remontées à travers ces consultations ont alimenté la définition des engagements, certaines sont incluses dans le plan d'action national sous forme de verbatim.
- Des ateliers-relais organisés par des associations et acteurs des territoires, notamment [La Cantine brestoise](http://www.lacantine-brest.net/) et [Décider ensemble](http://www.deciderensemble.com/page/9519-)
- Des entretiens bilatéraux et sessions de travail avec des associations et groupes d'experts (voir la [liste des organisations](documentation/personnalites_rencontrees_2.1.md)) 
- Des réunions, ateliers et événements contributifs ouverts à tous et organisés dans toute la France (voir tableau ci-dessous)
- La mobilisation du [réseau d'experts d'Etalab](https://www.etalab.gouv.fr/reseau-dexperts), composé de personnalités qualifiées issues de la société civile (associations citoyennes, recherche, entreprises, open data)
- Des propositions formulées dans de nombreux rapports publics et provenant d'instituts de recherche et de think tanks
- Des présentations et points d'étapes réalisés avec les acteurs publics et la société civile (liens vers les présentations des points d'étape ci-dessous)

Ces interactions ont permis de sensibilier de nombreux acteurs aux objectifs et aux principes du gouvernement ouvert et d'enrichir les premières pistes d'engagements. La mobilisation de nombreux ministères dans la démarche a également permis une forte appropriation des engagements, accélérant ainsi l'agenda de l'ouverture.
Voir la [liste des ministères et administrations](documentation/administrations_pan.md) engagés dans le Plan d'action.




## Evénements contributifs   
    
**Evénement** | **Date et lieu** | **Thème** | **Organisation** 
--- | --- | --- | ---
Open World Forum | 31/10/2014 à Paris | Atelier : [“From Open Data to Open Gov, the role of the Civil Society”](http://www.openworldforum.paris/fr/tracks/from-open-data-to-open-gov-the-role-of-the-civil-society-workshop-) | Animation par Etalab
Semaine de l'innovation publique | 15/11/2014 à Paris | Atelier : [« Gouvernement ouvert : comment l’Etat peut-il faire progresser la transparence de l’action publique et la participation citoyenne ?»](http://www.modernisation.gouv.fr/semaine-innovation-publique/programme/cite-innovation-publique) | Animation par Etalab et le CNNum
Institut d'études politiques | 20/11/2014 à Lille | Ateliers organisés par des étudiants en Master "Management des politiques publiques" | IEP
Atelier-relais | 16/12/2014 à Brest | [Open gov : comment faire progresser la transparence de l'action publique et la participation citoyenne](https://openagenda.com/events/atelier-relais-demi-journee-contributive-a-brest) | La Cantine brestoise
Atelier-relais | 18/12/206 à Paris | [Open data : vers une démocratie plus ouverte ?](https://openagenda.com/events/decider-ensemble-atelier-sur-la-societe-face-a-la-metamorphose-numerique-palais-bourbon?lang=fr) | Décider ensemble
Journée contributive concertation "Ambition numérique" | 19/01/2015 à Bordeaux | Ateliers ["gouvernement ouvert dans les territoires" et "ouverture des données et des modèles publics"](https://contribuez.cnnumerique.fr/actualite/vid%C3%A9o-retour-sur-la-journ%C3%A9e-contributive-n%C2%B03-%E2%80%9Cla-transformation-num%C3%A9rique-de-l%E2%80%99action) | Animation par Etalab
Séminaires « G90 » et « entreprenariat durable et innovation »| 21/01/2015 à l'Université technologique de Compiègne | Atelier participatif avec les étudiants " Comment renouveler le débat public à l'ère du numérique" | Animation par les enseignants de l'université, la Commission nationale du débat public (CNDP), Etalab
Atelier-relais | 26/01/2015 à Paris | [Open gov : comment faire progresser la transparence de l'action publique et la participation citoyenne ?](https://openagenda.com/contribuez/events/atelier-relais-de-france-strategie?lang=fr) | France Stratégie
Forum ouvert | 19/05/2015 à Paris | Forum ouvert [" Comment rénover et pérenniser les modes de coopération entre administration et citoyen ?"](http://www.participation-et-democratie.fr/fr/content/comment-renover-et-perenniser-les-modes-de-cooperation-entre-administration-et-citoyen) | CNDP et Etalab


## Présentations des points d'étape   
    
- [Mardi 16 décembre 2014 à Numa](http://fr.slideshare.net/Etalab/presentation-point-etape-1-ogp)
- [Mardi 17 février 2015 à Superpublic](http://fr.slideshare.net/Etalab/support-point-dtape-pan-daction-national-ogp)
- [Mardi 17 mars 2015 à Numa](http://fr.slideshare.net/Etalab/20150317-ogp-point-dtape)








