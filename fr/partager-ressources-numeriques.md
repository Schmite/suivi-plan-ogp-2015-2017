# Partager des ressources numériques

- [Engagement 15 : Renforcer la politique d'ouverture et de circulation des données](Engagement15.md)
- [Engagement 16 : Favoriser l'ouverture des modèles de calcul et des simulateurs de l'État](Engagement16.md)
- [Engagement 17 : Transformer les ressources technologiques de l'État en plateforme ouverte](Engagement17.md)
- [Engagement 18 : Mieux interagir avec l'usager et améliorer les services publics grâce à l'administration numérique](Engagement18.md)